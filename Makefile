
ORIG = origin
CHECK = checked

all: compile

run: init
	./benchmarktk/run-benchmark setup.yaml

collect: init
	./benchmarktk/collect-data setup.yaml

pull: update

up: update

update: init
	git pull
	(cd jarmus; git pull)
	(cd $(CHECK); git pull)
	(cd $(ORIG); git pull)
	(cd stats-tk; git pull)

compile: init
	(cd jarmus; ant jar jar-inst)
	(cp jarmus/target/jarmus.jar jarmus/target/jarmusc.jar $(CHECK)/)
	(cd $(CHECK); make)
	(cd $(ORIG); make)

init: jarmus $(CHECK) $(ORIG) stats-tk benchmarktk \
	data/$(ORIG) data/$(CHECK) logs/$(ORIG) logs/$(CHECK)

data/$(ORIG):
	mkdir -p $@

data/$(CHECK):
	mkdir -p $@

logs/$(ORIG):
	mkdir -p $@

logs/$(CHECK):
	mkdir -p $@

jarmus:
	git clone https://bitbucket.org/cogumbreiro/jarmus/

$(CHECK):
	git clone https://bitbucket.org/cogumbreiro/jgf-mt/ $(CHECK)
	(cd $(CHECK); git checkout remote/jarmus)

$(ORIG):
	git clone https://bitbucket.org/cogumbreiro/jgf-mt/ $(ORIG)

stats-tk:
	git clone https://bitbucket.org/cogumbreiro/stats-tk/ stats-tk

benchmarktk:
	git clone https://bitbucket.org/cogumbreiro/benchmarktk/

clean: cleanlogs
	(cd jarmus; ant clean)
	(cd $(CHECK); make clean)
	(cd $(ORIG); make clean)
