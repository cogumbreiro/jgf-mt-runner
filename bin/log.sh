#!/bin/bash
format() {
  grep 'Section.:' | awk '{print $1","$2","$4}'
}

merge() {
  cat section1/run.log | format
  cat section2/run3.log | format
  cat section3/run2.log | format | grep Total:
}
orig=$(mktemp) || exit 255
jarmus=$(mktemp) || (rm -f $orig; exit 255)
(cd jgf-mt; merge) > $jarmus &&
(cd jgf-mt-orig; merge) > $orig &&
python $(dirname "$0")/merge.py $orig $jarmus
rm -f $orig $jarmus
