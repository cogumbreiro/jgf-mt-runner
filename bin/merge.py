import csv

def main():
    import sys
    orig = csv.reader(open(sys.argv[1]))
    jarmus = csv.reader(open(sys.argv[2]))
    out = csv.writer(sys.stdout)
    merge(orig, jarmus, out)

def merge(orig, jarmus, out):
    for (o_row, j_row) in zip(orig, jarmus):
        label = o_row[0]
        o_time = o_row[1]
        j_time = j_row[1]
        o_ops = o_row[2]
        j_ops = j_row[2]
        slowdown = int((1 - float(o_time) / float(j_time)) * 100)
        row = (label, o_time, j_time, o_ops, j_ops, slowdown)
        out.writerow(row)

if __name__ == '__main__':
    main()
